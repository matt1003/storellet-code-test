import React from "react";
import { StyledAppInfo, StyledId, AppIcon, AppDetail } from "./AppInfo.style";

function AppInfo({ children, id, label, image, onClick }) {
  return (
    <StyledAppInfo onClick={onClick}>
      <StyledId>{id}</StyledId>
      <AppIcon id={id} src={image} alt="new" />
      <AppDetail>
        <b>{children}</b>
        <div style={{ fontSize: 14, marginTop: 5 }}>{label}</div>
      </AppDetail>
    </StyledAppInfo>
  );
}

export default AppInfo;
