import styled from "styled-components";
export const StyledAppInfo = styled.div`
  background-color: white;
  display: flex;
  flex-direction: row;
  border: 0px;
  border-radius: 5px;
  margin-top: 10px;
  padding: 1em;
  align-items: center;
`;

export const StyledId = styled.div`
  font-size: 18px;
  padding-left: 5px;
  padding-right: 20px;
`;

export const AppIcon = styled.img`
  width: 60px;
  height: 60px;
  border-radius: ${(props) => (props.id % 2 === 0 ? "30px" : "15px")};
`;

export const AppDetail = styled.div`
  padding-left: 20px;
  text-align: left;
`;
