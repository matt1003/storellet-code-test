import React from "react";
import { StyledAppInfo, AppIcon, AppDetail } from "./RecommandedAppInfo.style";

function RecommandedAppInfo({ children, label, image, onClick }) {
  return (
    <StyledAppInfo onClick={onClick}>
      <AppIcon src={image} alt="new" />
      <AppDetail style={{ marginTop: 5 }}>
        <b>{children}</b>
        <div style={{ fontSize: 14, marginTop: 5 }}>{label}</div>
      </AppDetail>
    </StyledAppInfo>
  );
}

export default RecommandedAppInfo;
