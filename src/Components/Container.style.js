import styled from "styled-components";

export const AppContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

export const Header = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  padding-top: 6px;
  padding-bottom: 6px;
  display: flex;
  justify-content: flex-end;
  background-color: lightblue;
`;
