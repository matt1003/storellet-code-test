import styled from "styled-components";

export const StyledAppInfo = styled.div`
  flex: none;

  display: flex;
  flex-direction: column;

  background-color: white;
  width: 120px;
  border-radius: 9px;
  margin-right: 10px;
  padding: 1em;
  align-items: center;
`;

export const AppIcon = styled.img`
  width: 90%;
  height: auto;
  border-radius: 15px;
  margin-bottom: 12px;
`;

export const AppDetail = styled.div`
  font-size: 14px;
  text-align: left;
  align-self: flex-start;
`;

export const AppTitle = styled.div`
  padding-top: 15px;
  font-weight: bold;
`;
