import { AppContainer, Header } from "./Components/Container.style";
import AppInfo from "./Components/AppInfo";
import RecommandedAppInfo from "./Components/RecommandedAppInfo";
import React, { useState, useEffect } from "react";

function App() {
  const [appList, setAppList] = useState([]);
  const [searchKeyword, setSearchKeyword] = useState("");
  const [recommendList, setRecommandList] = useState([]);

  const getAppList = async () => {
    try {
      let response = await fetch(
        "https://itunes.apple.com/hk/rss/topfreeapplications/limit=100/json"
      );
      return await response.json();
    } catch (error) {
      console.error(error);
    }
  };

  const getRecommandAppList = async () => {
    try {
      let response = await fetch(
        "https://itunes.apple.com/hk/rss/topgrossingapplications/limit=10/json"
      );
      return await response.json();
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    async function fetchData() {
      const appList = await getAppList();
      setAppList(appList.feed.entry);

      const recommendList = await getRecommandAppList();
      setRecommandList(recommendList.feed.entry);
    }
    fetchData();
  }, []);

  const filterList = (list, searchKeyword) => {
    return list.filter((e) => {
      return (
        e["im:name"].label.includes(searchKeyword) ||
        e.category.attributes.label.includes(searchKeyword) ||
        e.summary.label.includes(searchKeyword) ||
        e["im:artist"].label.includes(searchKeyword)
      );
    });
  };

  return (
    <>
      <AppContainer className="page-margin">
        <h2 style={{ marginTop: 60 }}>推介</h2>
        <div
          className="no-scrollbar"
          style={{
            display: "flex",
            width: "100%",
            overflow: "auto",
          }}
        >
          {filterList(recommendList, searchKeyword).map((e, i) => {
            return (
              <RecommandedAppInfo
                key={i}
                image={e["im:image"][0].label}
                label={e.category.attributes.label}
                onClick={() => {
                  console.log(e);
                  console.log(JSON.stringify(e));
                }}
              >
                {e["im:name"].label}
              </RecommandedAppInfo>
            );
          })}
        </div>

        {filterList(appList, searchKeyword).map((e, i) => {
          return (
            <AppInfo
              key={i}
              id={i + 1}
              image={e["im:image"][0].label}
              label={e.category.attributes.label}
              onClick={() => {
                console.log(e);
                console.log(JSON.stringify(e));
              }}
            >
              {e["im:name"].label}
            </AppInfo>
          );
        })}
      </AppContainer>
      <Header className="page-margin">
        <input
          style={{ width: "100%", padding: "8px 10px" }}
          placeholder="Type something in here to search ..."
          type="textarea"
          name="textValue"
          value={searchKeyword}
          onChange={(e) => setSearchKeyword(e.target.value)}
        />
      </Header>
    </>
  );
}

export default App;
